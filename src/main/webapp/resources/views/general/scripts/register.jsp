<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<script
	src="${ pageContext.request.contextPath }/resources/views/general/js/datatimepicker.js"></script>

<script>
	var countries;

	$
			.ajax({
				dataType : "json",
				url : "${ pageContext.request.contextPath }/resources/json/countries.json",
				success : function(response) {
					countries = response;
				}
			});

	YUI().use('array-extras', 'autocomplete', 'autocomplete-highlighters',
			function(Y) {
				Y.one('#regCountry').plug(Y.Plugin.AutoComplete, {
					source : getCountriesByQuery
				});

			});

	function getCountriesByQuery(query, callback) {
		var ret = [];
		query = query.toLowerCase();
		for ( var i in countries) {
			if (countries[i].toLowerCase().indexOf(query) > -1) {
				ret.push(countries[i]);
				if (ret.length >= 5)
					break;
			}
		}

		callback(ret);
	}
</script>